#!/bin/bash

IP=$(curl ipinfo.io/ip)
echo $IP > /home/void/Dropbox/sshero

telegram-send $IP

while [ true ]; do

	sleep 10m
    NEW_IP=$(curl ipinfo.io/ip) 
	if [ "$IP" != "$NEW_IP" ]; then

        echo $NEW_IP > /home/void/Dropbox/sshero
		telegram-send $NEW_IP;
	    $IP=$NEW_IP
    fi
done
